package th.ac.tu.siit.calculator;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity
	implements OnClickListener {
	
	
	
	
	String oper; 

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        Button add = (Button)findViewById(R.id.add);
		add.setOnClickListener(this);
		
		((Button)findViewById(R.id.sub)).setOnClickListener(this);
		((Button)findViewById(R.id.mul)).setOnClickListener(this);
		((Button)findViewById(R.id.div)).setOnClickListener(this);
		
		((Button)findViewById(R.id.num0)).setOnClickListener(this);
		((Button)findViewById(R.id.num1)).setOnClickListener(this);
		((Button)findViewById(R.id.num2)).setOnClickListener(this);
		((Button)findViewById(R.id.num3)).setOnClickListener(this);
		((Button)findViewById(R.id.num4)).setOnClickListener(this);
		((Button)findViewById(R.id.num5)).setOnClickListener(this);
		((Button)findViewById(R.id.num6)).setOnClickListener(this);
		((Button)findViewById(R.id.num7)).setOnClickListener(this);
		((Button)findViewById(R.id.num8)).setOnClickListener(this);
		((Button)findViewById(R.id.num9)).setOnClickListener(this);
		
		((Button)findViewById(R.id.ac)).setOnClickListener(this);
		((Button)findViewById(R.id.bs)).setOnClickListener(this);
		
		((Button)findViewById(R.id.dot)).setOnClickListener(this);
		((Button)findViewById(R.id.equ)).setOnClickListener(this);
		
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    double d1 = 0;
    String output = "0";
    int sign = 0;
    double d2 = 0 ;
    
    void compute(){
    	d2 = Double.parseDouble(output);
    	double ans = 0;
    	if (sign == 1){
    		
    		ans = d1 + d2;
    		
    	}
    	else if (sign == 2){
    		
    		ans = d1 - d2;
    	}
    	else if (sign == 3){
    		
    		ans = d1 * d2;
    	}
    	else if (sign == 4){
    		
    		ans = d1 / d2;
    	}
    	
    	output = ans + "";
    	d1 = ans;
    	
    	
    }

	@Override
	public void onClick(View v) {
		int id = v.getId();
		
		
		TextView tvOutput = (TextView)findViewById(R.id.output);
		TextView op = (TextView)findViewById(R.id.operator);
		//String output = tvOutput.getText().toString();
		
		switch(id){
		
		case R.id.num0:
		case R.id.num1:
		case R.id.num2:
		case R.id.num3:
		case R.id.num4:
		case R.id.num5:
		case R.id.num6:
		case R.id.num7:
		case R.id.num8:
		case R.id.num9:
			if(output.equals("0")){
				output = "";
				
			}
			
			output = output + ((Button)v).getText().toString();
			if (output.charAt(0) == '0')
				output = output.substring(1);

				if (output.length() <= 0)
				output = "0";

			tvOutput.setText(output);
			
			break;
		
		
		case R.id.add:
		
			
			
			
			
			if (sign == 0){
				sign = 1;
				d1 = Double.parseDouble(output);
				output = "0";
				// outputTV.setText(output);
				op.setText("ADD"); 
				}
				else
				{
				
				compute();
				tvOutput.setText(output);
				

				sign = 1;
				d1 = Double.parseDouble(output);
				output = "0";
				// outputTV.setText(output);
				op.setText("ADD"); 
				}
				break;
			
			
			
		
				
		
		case R.id.sub:	
			if (sign == 0){
				sign = 2;
				d1 = Double.parseDouble(output);
				output = "0";
				// outputTV.setText(output);
				op.setText("SUB"); 
				}
				else
				{
				
				compute();
				tvOutput.setText(output);
				

				sign = 2;
				d1 = Double.parseDouble(output);
				output = "0";
				// outputTV.setText(output);
				op.setText("SUB"); 

				}
				break;
			
		case R.id.mul:
			if (sign == 0){
				sign = 3;
				d1 = Double.parseDouble(output);
				output = "0";
				// outputTV.setText(output);
				op.setText("MUL"); 
				}
				else
				{
				
				compute();
				tvOutput.setText(output);
				

				sign = 3;
				d1 = Double.parseDouble(output);
				output = "0";
				// outputTV.setText(output);
				op.setText("MUL"); 

				}
				break;
			
		case R.id.div:
			if (sign == 0){
				sign = 4;
				d1 = Double.parseDouble(output);
				output = "0";
				// outputTV.setText(output);
				op.setText("DIV"); 
				}
				else
				{
				
				compute();
				tvOutput.setText(output);
				

				sign = 4;
				d1 = Double.parseDouble(output);
				output = "0";
				// outputTV.setText(output);
				op.setText("DIV"); 

				}
				break;
			
		case R.id.equ:
			compute();
			tvOutput.setText(output);
			op.setText("");
			output = "0";

			break; 
			
	
		}
		
		
	}
    
}
